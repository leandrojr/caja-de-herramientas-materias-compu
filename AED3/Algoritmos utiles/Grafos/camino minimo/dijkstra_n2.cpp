#include <iostream>
#include <vector>
// Prim en O(n^2)

using namespace std;
using Nodo = int;
using Costo = long long;
using Arista = pair< Nodo, Nodo >;

struct Vecino{
    Vecino(Nodo n, Costo c){
        nodo= n;
        costo= c;
    }
    Nodo nodo;
    Costo costo;
}; 

using Vecindario= vector<Vecino>;


long long inf = 1e18;
int n, m;
vector<Vecindario> g;

vector<Costo> distancias;

// empezamos de v0
Nodo v0;

void dijkstra_n2(){

    distancias= vector<Costo>(n, inf);

    distancias[v0]= 0;
    vector<bool> enArbol(n, false);
    enArbol[v0] = true;

    for(Vecino v : g[v0])
        distancias[v.nodo] = v.costo;
    
    for(int it = 0; it < n-1; it++){
        // queremos la arista de menor peso de las que están en la frontera

        //indice del más cercano
        int masCercano = -1;

        //O(n) ?  Busco la arista más liviana
        for(int i = 0; i < n; i++){ // nodos candidatos a agregar
            if(!enArbol[i]){
                // elegimos la arista de menor distancia a v0
                if(masCercano == -1 || distancias[i] < distancias[masCercano])
                    masCercano = i;
            }
        }
        if(distancias[masCercano] == inf){
            cout << "g no es conexo, tengo aristas no alcanzables desde v0" << endl;
            return;
        }

        enArbol[masCercano] = true;


        //O(n) ? Actualizo las distancias de los vecinos de la arista más cercana a v0

        for(Vecino w : g[masCercano]){

            if(!enArbol[w.nodo]){
                if(w.costo < distancias[w.nodo])
                    distancias[w.nodo] = w.costo + distancias[masCercano];
            }
        }
    }
    return;
}

int main(){
    bool esDigrafo = true;

    cin >> n >> m;

    g = vector<Vecindario>(n);

    for(int i = 0; i < m; i++){
        //arista (u,v) y vosto w
        int u, v, w;
        cin >> u >> v >> w;
        u--; v--;
        if(!esDigrafo)
            g[v].push_back(Vecino(u,w));
        g[u].push_back(Vecino(v,w));
    }

    //el nodo origen
    v0=1;
    v0--;

    dijkstra_n2();

    //imprimo el resultado
    for(Costo c : distancias)
        cout<<c<<" ";
    cout<<endl;

}