/*Algoritmo de Belman Ford Bottom-up */
#include <bits/stdc++.h>
#include <climits>
#include <iostream>
#include <vector>

using namespace std;
using Nodo = int;
using Costo = long long;
struct Vecino{
    Vecino(Nodo n, Costo c){
        nodo= n;
        costo= c;
    }
    Nodo nodo;
    Costo costo;
}; 
using Vecindario= vector<Vecino>;
using Peso = int;
using Arista = pair< Nodo, Nodo >;

const Peso INFTY = INT_MAX;

int n,m;

Peso suma(Peso p, Peso q) {
    if (p == INFTY || q == INFTY) 
        return INFTY;
    else 
        return p + q;
}

vector<Vecindario> G;

vector<Costo> BellmanFord(vector<Vecindario> &G, Nodo v0){
    int n= G.size();

    //el vector donde guardo el camino mínimo desde el nodo v0
    vector<Costo> dist = vector<Costo>(n, INFTY);

    //fijo a v0 como referencia
    dist[v0] = 0;
    bool cambio= true;

    //cuento la cantidad de iteraciones para detectar ciclos de longitud negativa
    int cantIteraciones=0;
    while(cambio && cantIteraciones<n){
        cantIteraciones++;
        cambio= false;

        //recorro todo el vector cambiando los valores si encuentro un mejor camino
        for(Nodo v=0; v<n ; v++){
            //actualizo el camino minimo hasta e.nodo si encuentro uno mejor
            for(Vecino e : G[v]){
                //el costo de llegar hasta e.nodo a través de la arista v
                Peso dist2= suma(dist[v], e.costo);
                if(dist[e.nodo]>dist2){
                    cambio= true;
                    dist[e.nodo]= dist2;
                }
            }
        }
    }

    if(cantIteraciones==n)
        cout<< "Hay ciclo de longitud negativa"<<endl;
    
    return dist;
}

int main(){
    bool esDigrafo= true;

    cin>>n>>m;
    
    G = vector<Vecindario>(n);

    //elijo desde que nodo calcular las distancias
    Nodo v0= 1;
    v0--;

    for(int i = 0; i < m; i++){
        int u,v; Costo c;
        cin>>u>>v>>c;
        u--, v--;

        if(!esDigrafo)
            G[v].push_back(Vecino(u,c));
        G[u].push_back(Vecino(v,c));
    }


    vector<Costo> caminoMinimo= BellmanFord(G, v0);

    //imprimo el resultado
    for(Costo elem : caminoMinimo)
        cout<<elem<<" ";
    cout<<endl;

    return 0;
}











