#include <iostream>
#include <vector>
#include <climits>

using namespace std;
using Costo = int;
using Nodo = int;
using Matriz = vector<vector<Costo>>;

const Costo INF = INT_MAX;

Costo suma(Costo p, Costo q) {
    if (p == INF || q == INF) 
        return INF;
    else 
        return p + q;
}

void floyd(Matriz& G){
    int n= G.size();
    //considerando a k como nodo intermedio
    for(Nodo k=0; k<n ;k++){
        for(Nodo i=0; i<n ;i++)
            for(Nodo j=0; j<n ;j++)
                //consindero la arista ij , veo si el costo de pasar por k es menor
                G[i][j]= min( G[i][j], suma( G[i][k] ,G[k][j]) );
    }

}


int main() {
    bool esDigrafo= true;
    int n,m;
    cin>>n>>m;
    
    //el grafo es una matriz de adyacencias con el costo (infinito si no existe el arco)
    Matriz G = Matriz(n,vector<Costo>(n,INF));

    for(int i=0; i<n ;i++)
        G[i][i]= 0;
    
    for(int i = 0; i < m; i++){
        int u,v; Costo c;
        cin>>u>>v>>c;
        u--, v--;

        if(!esDigrafo)
            G[v][u]= c;
        G[u][v]= c;
    }


    floyd(G);

    //imprimo el resultado
    cout<<endl;
    for(vector<Costo> fila : G){
        for(Costo c : fila)
            cout<<c<<" ";
        cout<<endl;
    }

    return 0;
}
