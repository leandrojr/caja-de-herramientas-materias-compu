#include <bits/stdc++.h>
#include <iostream>
#include <set>

using namespace std;
using Nodo = int;
using Costo = long long;
//using Vecino = pair<Costo, Nodo>; //pares costo(la distancia hasta ese nodo), numero de nodo
struct Vecino{
    Vecino(Nodo n, Costo c){
        nodo= n;
        costo= c;
    }
    Nodo nodo;
    Costo costo;
}; 

using Vecindario= vector<Vecino>;
using Arista = pair<Nodo,Nodo>;

class Cola{
    public:
        Cola(){
            str= set<pair<Costo,Nodo>>();
        }
        void encolar(Costo c, Nodo n){
            str.insert(make_pair(c,n));
        }

        pair<Costo,Nodo> desencolar(){
            pair<Costo,Nodo> res= *str.begin();
            str.erase(*str.begin());
            return res;
        }

        void eliminar(Costo c, Nodo n){
            if(str.count(make_pair(c,n))>0)   
                str.erase(make_pair(c,n));
        }

        bool vacio(){
            return str.empty(); 
        }

    private:
         set< pair<Costo,Nodo>> str;
};

const Costo INF = 1e18;
int n, m;

vector<Vecindario> g;


//Implementacion prim , m log n
vector<Costo> djs_m_lg_n(){

    //agrego a prim
    vector<Costo> caminoMinimo(n,INF);

    //priority_queue<pair<Costo, Arista>> q;
    //para que eliminar sea log necesito una cola de prioridad implementada como un avl
    Cola q;

    vector<bool> visited(n, false);

    Nodo origen= 0;
    caminoMinimo[origen]= 0;

    for(Vecino v : g[origen]){
        q.encolar(v.costo , v.nodo);
        caminoMinimo[v.nodo]= v.costo;
    }
        

    visited[origen] = true;
    int aristas = 0;
    
    while(!q.vacio()){
        Costo costo;
        Nodo nodo;
        tie(costo, nodo) = q.desencolar();
        if(!visited[nodo]){
            visited[nodo] = true;
            for(Vecino v : g[nodo]){
                Costo dist = caminoMinimo[nodo] + v.costo;  
                if(caminoMinimo[v.nodo]>dist){
                    q.eliminar(caminoMinimo[v.nodo],v.nodo);
                    q.encolar(dist,v.nodo);
                    caminoMinimo[v.nodo]=dist;
                }
            }

            aristas ++;
        }
    }
    if(aristas != n-1)
        cout << "IMPOSSIBLE" << endl;
    
    return caminoMinimo;
}

int main() {
    bool esDigrafo= true;

    cin>>n>>m;
    
    g = vector<Vecindario>(n);

    for(int i = 0; i < m; i++){
        int u,v; Costo c;
        cin>>u>>v>>c;
        u--, v--;

        if(!esDigrafo)
            g[v].push_back(Vecino(u,c));
        g[u].push_back(Vecino(v,c));
    }
    vector<Costo> caminoMinimo= djs_m_lg_n();

    //imprimo el resultado
    for(Costo elem : caminoMinimo)
        cout<<elem<<" ";
    cout<<endl;

    return 0;
}
