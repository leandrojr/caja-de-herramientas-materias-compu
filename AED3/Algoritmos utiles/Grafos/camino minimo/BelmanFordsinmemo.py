#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 13 15:09:56 2023

@author: leandro

Bellman-Ford sin memo
"""

inf = 10e6

class Vecino:
    def __init__(self, nodo, costo, esincidente):
        self.nodo= nodo
        self.costo= costo
        self.esincidente= esincidente

G=([Vecino(1, 4, False),Vecino(5, 3, False),Vecino(2, 7, False)],
   [Vecino(0, 4, True),Vecino(5, -2, False),Vecino(4, 1, False),Vecino(2, 3, False)],
   [Vecino(1, 3, True),Vecino(0, 7, True),Vecino(3, 1, False),Vecino(4, 1, False)],
   [Vecino(2, 1, True),Vecino(4, 4, True)],
   [Vecino(2, 1, True),Vecino(1, 1, True),Vecino(5, 3, True),Vecino(3, 4, False)],
   [Vecino(0, 3, True),Vecino(1, -2, True),Vecino(4, 3, False)])


#distancia de v a w utilizando a lo sumo k arcos
def distancia(v,w,k):
    if k>0 and v==w:
        return 0
    elif k==0 and v!=w:
        return inf
    else:
        #caso recursivo
        minimo= inf
        for arista in G[w]:
            if arista.esincidente and distancia(v,arista.nodo,k-1)+arista.costo < minimo :
                minimo= distancia(v,arista.nodo,k-1)+arista.costo
        return minimo
    
distancias=[]
for i in range(len(G)):
    distancias.append(distancia(0,i,len(G)-1))
print(distancias)










