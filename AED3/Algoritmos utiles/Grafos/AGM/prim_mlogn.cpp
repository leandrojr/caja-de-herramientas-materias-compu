#include <bits/stdc++.h>
#include <iostream>

using namespace std;
using Nodo = int;
using Costo = long long;
using Vecino = pair<Costo, Nodo>; //pares costo(la distancia hasta ese nodo), numero de nodo
using Vecindario= vector<Vecino>;
using Arista = pair<Nodo,Nodo>;

const Costo INF = 1e18;
int n, m;

vector<Vecindario> g;

//Implementacion prim , m log n
void prim_m_lg_n(){
    priority_queue<pair<Costo, Arista>> q;
    vector<bool> visited(n, false);

    for(Vecino v : g[0])
        q.push(make_pair(- v.first , make_pair(0, v.second)));

    visited[0] = true;
    int aristas = 0;
    Costo s = 0;
    while(!q.empty()){
        Costo costo;
        Arista arista;
        tie(costo, arista) = q.top();
        q.pop();
        if(!visited[arista.second]){
            visited[arista.second] = true;
            for(Vecino v : g[arista.second])
                q.push(make_pair(-v.first, make_pair(arista.second, v.second)));
            
            s += -costo; //el costo de la arista v
            aristas ++;
        }
    }
    if(aristas == n-1){
        cout << s << endl;
    } else {
        cout << "IMPOSSIBLE" << endl;
    }
}

int main() {
    cin>>n>>m;
    
    g = vector<Vecindario>(n);

    for(int i = 0; i < m; i++){
        int u,v; Costo c;
        cin>>u>>v>>c;
        u--, v--;
        g[v].push_back({c,u});
        g[u].push_back({c,v});
    }
    prim_m_lg_n();
    return 0;
}
