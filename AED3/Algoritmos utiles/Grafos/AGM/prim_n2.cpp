#include <iostream>
#include <vector>
// Prim en O(n^2)

using namespace std;
using Nodo = int;
using Costo = long long;
using Arista = pair< Nodo, Nodo >;

struct Vecino{
    Vecino(Nodo n, Costo c){
        nodo= n;
        costo= c;
    }
    Nodo nodo;
    Costo costo;
}; 

using Vecindario= vector<Vecino>;



long long inf = 1e18;
int n, m;
vector<Vecindario> g;

// (u, v) está en la frontera si u están
void prim(){
    // empezamos de 0
    vector<bool> enArbol(n, false);
    enArbol[0] = true;

    vector<Costo> distancias(n, inf);
    //si también me intersa guardar las aristas del árbol además la distancia me guardo la arista correspondiente
    vector<Arista> arbolRes;

    for(Vecino v : g[0])
        distancias[v.nodo] = v.costo;
    

    Costo costoTotal= 0;
    for(int it = 0; it < n-1; it++){
        // queremos la arista de menor peso de las que están en la frontera

        //indice del más cercano
        int masCercano = -1;

        //O(n) ?  Busco la arista más liviana
        for(int i = 0; i < n; i++){ // nodos candidatos a agregar
            if(!enArbol[i]){
                // queremos que distancia[i] sea el peso de la arista más chica de i a un nodo del árbol (si no existe, inf)
                if(masCercano == -1 || distancias[i] < distancias[masCercano])
                    masCercano = i;
            }
        }
        if(distancias[masCercano] == inf){
            cout << "IMPOSSIBLE" << endl;
            return;
        }

        enArbol[masCercano] = true;
        costoTotal += distancias[masCercano];


        //O(n) ? Actualizo las distancias de los vecinos de la arista más liviana

        //variable solo si quiero guardar una lista de aristas, incorpore permite que se agregue una arista durante todo el ciclo
        bool incorpore= false;
        for(Vecino w : g[masCercano]){

            //este if es optativo, solo necesario si se desea una lista de aristas
            if(enArbol[w.nodo] && w.costo==distancias[masCercano] && !incorpore){
                arbolRes.push_back(make_pair(w.nodo, masCercano));
                incorpore= true;
            }

            //codigo obligatorio del algoritmo
            if(!enArbol[w.nodo]){
                if(w.costo < distancias[w.nodo])
                    distancias[w.nodo] = w.costo;
            }
        }
    }

    //imprimir el costo del ábol
    cout << costoTotal << endl;

    //imprimir las aristas, si me interesa
    for(Arista a : arbolRes){
        cout<<"("<<a.first+1<<","<<a.second+1<<")"<<" "<<endl;
    }
    cout<<endl;
}

int main(){
    cin >> n >> m;

    g = vector<Vecindario>(n);

    for(int i = 0; i < m; i++){
        //arista (u,v) y vosto w
        int u, v, w;
        cin >> u >> v >> w;
        u--; v--;
        g[v].push_back(Vecino(u,w));
        g[u].push_back(Vecino(v,w));
    }

    prim();

}